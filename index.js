const express = require('express')
const FileService = require('./fileService')

const app = express()
const port = process.env.PORT || 8080

app.use(express.json())


app.post('/api/files', (req, res) => {
    console.log(req.method, req.url, req.body)
    const filename = req.body.filename
    const content = req.body.content
    const fileService = new FileService(filename, content)

    try {
        if (fileService.isValidFile(filename) || (!req.body.filename || !req.body.content)) {
            return res.status(400).send({"message": "Please specify valid parameters"})
        }

        if (fileService.fileExists(filename)) {
            return res.status(400).send({"message": "Such file already exists"})
        }

        fileService.createFile()
        res.status(200).send({"message": "File created successfully"})
    } catch (error) {
        return res.status(500).send({"message": "Server error"})
    }
})


app.get('/api/files', (req, res) => {
    console.log(req.method, req.url)
    const fileService = new FileService()

    try {
        const files = fileService.getFiles()

        if (files.length === 0) {
            return res.status(400).send({message: "There are no uploaded files yet"})
        }

        res.status(200).send({
            "message": 'Success',
            files
        })
    } catch (error) {
        res.status(500).send({"message": "Server error"})
    }
})


app.get('/api/files/:filename', (req, res) => {
    console.log(req.method, req.url, req.params)
    const filename = req.params.filename
    const fileService = new FileService(filename)

    try {
        if (!fileService.fileExists(filename)) {
            return res.status(400).send({"message": `No file with ${filename} filename found`})
        }
        if(fileService.isFileToBeIgnored(filename)){
            return res.status(400).send({"message": `No uploaded file with ${filename} filename found`})
        }

        const message = "Success"
        const content = fileService.getFileContent()
        const extension = fileService.getExtension()
        const uploadedDate = fileService.createdDate()

        const response = {
            message,
            filename,
            content,
            extension,
            uploadedDate
        }

        res.status(200).send(response)
    } catch (error) {
        res.status(500).send({"message": "Server error"})
    }
})


app.patch('/api/files/:filename', (req, res) => {
    console.log(req.method, req.url, req.params)
    const filename = req.params.filename
    const fileService = new FileService(filename)

    const newFilename = req.body.filename
    const newContent = req.body.content

    if(!newFilename && !newContent){
        return res.status(400).send({"message": `No body`})
    }
    if(!fileService.fileExists(filename)){
        return res.status(400).send({"message": `No`})
    }
    try {
        if (fileService.isFileToBeIgnored(filename) || fileService.isFileToBeIgnored(newFilename) ) {
            return res.status(400).send({"message": `${filename} or ${newFilename} is forbidden`})
        }
        if (fileService.fileExists(newFilename) || !fileService.fileExists(filename)) {
            return res.status(400).send({"message": `No file with ${filename} filename found`})
        }
        if (fileService.isValidFile(newFilename)){
            return res.status(400).send({"message": "Invalid name of the file"})
        }

        if (newContent) {
            fileService.editFileContent(newContent)
        }
        if (newFilename) {
            fileService.editFileName(newFilename)
        }
        res.status(200).send({"message": "success"})
    } catch (error) {
        res.status(500).send({"message": "server error"})
    }

})

app.delete('/api/files/:filename', (req, res) => {
    console.log(req.method, req.url, req.params)
    const filename = req.params.filename
    const fileService = new FileService(filename)

    try {
        if (!fileService.fileExists(filename)) {
            return res.status(400).send({"message": `No file with ${filename} filename found`})
        }
        if(fileService.isFileToBeIgnored(filename)){
            return res.status(400).send({"message": `${filename} cannot be changed `})
        }
        fileService.deleteFile()
        res.status(200).send({"message": "success"})
    } catch (error) {
        res.status(500).send({"message": "server error"})
    }
})

app.listen(port, () => {
    console.log(`Server is up on port: ${port}`)
})
