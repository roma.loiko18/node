const fs = require("fs");
const path = require("path");

class FileService {

    directoryPath = path.join(__dirname)
    availableFilesRegexp = (/\.(log|txt|json|yaml|xml|js)$/i)
    filesToBeIgnored = ['index.js', '.gitignore', 'package.json', 'package-lock.json', 'README.md', 'fileService.js']


    constructor(filename, content) {
        this.filename = filename
        this.content = content
    }

    getExtension() {
        const parts = this.filename.split('.')
        return parts[parts.length - 1]
    }

    createdDate() {
        const {birthtime} = fs.statSync(`${this.directoryPath}/${this.filename}`)
        return birthtime
    }

    isFileToBeIgnored = (file) => {
        return this.filesToBeIgnored.includes(file)
    }

    isFolder(file) {
        return fs.lstatSync(`${this.directoryPath}/${file}`).isDirectory()
    }

    getAllFiles() {
        return fs.readdirSync(this.directoryPath);
    }

    getFiles() {
        const allFiles = fs.readdirSync(this.directoryPath);
        return allFiles.filter(file => !this.isFileToBeIgnored(file) && !this.isFolder(file))
    }

    fileExists(filename) {
        return this.getAllFiles().reduce((isUnique, file) => {
            return file === filename ? true : isUnique
        }, false)
    }

    isValidFile(filename) {
        return !this.availableFilesRegexp.test(filename)
    }

    createFile() {
        fs.writeFile(`${this.directoryPath}/${this.filename}`, `${this.content}`, (err) => {
                if (err) throw err;
            }
        );
    }

    getFileContent() {
        return fs.readFileSync(`${this.directoryPath}/${this.filename}`, 'utf8')
    }

    editFileContent(newContent) {
        fs.writeFile(`${this.directoryPath}/${this.filename}`, `${newContent}`, (err) => {
                if (err) throw err;
            }
        );
    }

    editFileName(newFilename) {
        fs.rename(`${this.directoryPath}/${this.filename}`, `${this.directoryPath}/${newFilename}`, (err) => {
            if (err) console.log('ERROR: ' + err);
        });
    }

    deleteFile() {
        fs.unlink(`${this.directoryPath}/${this.filename}`, (err) => {
            if (err) {
                throw new Error(err.message)
            }
        });
    }
}

module.exports = FileService